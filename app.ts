import { opine } from "https://deno.land/x/opine@1.7.2/mod.ts";

const app = opine();


app.post("/player", function (req, res) {
  res.send({
    "Name": "name",
    "Email": "email",
    "Mobile": "mobileNumber",
    "fcmtoken": "fcmToken",
    "Rid": "rid",
    "Password": "password",
    "OTP": "otp",
  });
});

app.get("/player", (req, res) => {
  res.send({
    "Name": "name",
    "Email": "email",
    "Mobile": "mobileNumber",
    "fcmtoken": "fcmToken",
    "Rid": "rid",
    "Password": "password",
    "OTP": "otp",
  });
});

app.post("/player/login", (req, res) => {
  res.send({
    "Mobile": "userid",
    "password": "password",
    "fcmtoken": "fcmToken",
  })
});

app.post("/player/loginbympin", (req, res) => {
  res.send({
    "MPIN": "mpin",
    "IMEI": "imei",
    "fcmtoken": "fcmToken",
  });
});

app.post("/player/passwordchange", (req, res) => {
  res.send({
    "password": "password",
  });
});

app.listen(3000, () => console.log("server has started on http://localhost:3000 🚀"));